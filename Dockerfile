FROM alpine:3.11

RUN apk --no-cache add curl

RUN apk --no-cache add ca-certificates
WORKDIR /usr/local/bin
ADD lab-assistant .
