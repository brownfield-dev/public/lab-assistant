# Lab Assistant [![pipeline status](https://gitlab.com/brownfield-dev/public/lab-assistant/badges/master/pipeline.svg)](https://gitlab.com/brownfield-dev/public/lab-assistant/-/commits/master)  [![coverage report](https://gitlab.com/brownfield-dev/public/lab-assistant/badges/master/coverage.svg)](https://gitlab.com/brownfield-dev/public/lab-assistant/-/commits/master)

A Golang executable that can run some commonly needed API calls against a GitLab instance. 

This is intended to mediate some more complex processes to improve flexibility and control without adding manual steps. 

Based on the GitLab Go API project on GitHub by Xanzy called "go-gitlab". 

`import "github.com/xanzy/go-gitlab"`

Running locally: 

`go run . --gl-token={PAT}`


Testing with GitLab.com: 

*  Project 1: 19762438 `Test Lab Assistant Dependency` 
*  Project 2: 19762418 `Test Lab Assistant`

In the dependency project, a job exists that runs on Tags. A new MR is opened against the Test lab Assistant project to upgrade the dependency to the new Tag. 

Example command to test... 

```bash
GITLAB_USER_ID=3548980 CI_PROJECT_ID=19762438 go run . --gl-token=[PERSONAL_ACCESS_TOKEN] --target-project-id=19762418
```