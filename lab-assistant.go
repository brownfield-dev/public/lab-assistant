package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"strings"

	b64 "encoding/base64"

	"github.com/xanzy/go-gitlab"
)

type Flags struct {
	glAddress          string
	glToken            string
	ciCommitSha        string
	ciCommitShort      string
	ciProjectId        int
	ciProjectName      string
	ciProjectNamespace string
	gitlabUserId       int
	targetProjectId    int
	targetUserId       int
	ciCommitTag        string
	ciRegistryImage    string
}

func main() {
	flags := new(Flags)
	// CI_API_V4_URL
	flag.StringVar(&flags.glAddress, "ci-api-v4-url", "https://gitlab.com/api/v4", "GitLab URL to API endpoint")
	flag.StringVar(&flags.glToken, "gl-token", "", "GitLab Access Token")
	flag.StringVar(&flags.ciCommitSha, "ci-commit-sha", "", "Commit SHA of tagged commit")
	flag.StringVar(&flags.ciCommitShort, "ci-commit-short-sha", "", "Short commit SHA of tagged commit")
	flag.IntVar(&flags.ciProjectId, "ci-project-id", 0, "Integer to identify dependency project")
	flag.StringVar(&flags.ciProjectName, "ci-project-name", "", "Human-readable project name")
	flag.StringVar(&flags.ciProjectNamespace, "ci-project-namespace", "", "Project group and name")
	flag.IntVar(&flags.gitlabUserId, "gitlab-user-id", 0, "Int identifier of the pipeline user")
	flag.IntVar(&flags.targetProjectId, "target-project-id", 0, "Int identifier of the destination project")
	flag.IntVar(&flags.targetUserId, "target-user-id", 0, "Int identifier of the user to assign the new MR to")
	flag.StringVar(&flags.ciCommitTag, "ci-commit-tag", "", "Commit tag if available")
	flag.StringVar(&flags.ciRegistryImage, "ci-registry-image", "", "Registry image if available")
	flag.Parse()
	err := SetFlagsFromEnv(flag.CommandLine)
	if err != nil {
		log.Fatalf("Failed to process environment variables: %v", err)
	}

	fmt.Println("API Url: ", flags.glAddress)
	fmt.Println("Token found: ", flags.glToken)
	fmt.Println("CI Commit Sha: ", flags.ciCommitSha, " and short: ", flags.ciCommitShort)
	fmt.Println("Commit Tag: ", flags.ciCommitTag)
	fmt.Println("Project identifiers =>  Source: ", flags.ciProjectId, " and Destination: ", flags.targetProjectId)
	fmt.Println("Project source namespace: ", flags.ciProjectNamespace)
	fmt.Println("User IDs => Triggered by: ", flags.gitlabUserId, " and assignee: ", flags.targetUserId)
	fmt.Println("End of input flags")

	git, err := gitlab.NewClient(flags.glToken, gitlab.WithBaseURL(flags.glAddress))
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}

	mr, err := makeNewMR(flags, git)
	if err != nil {
		log.Fatalf("Failed to create new mr or branch: %v", err)
	}

	_, err = fileEditCommit(flags, git, mr)
	if err != nil {
		log.Fatalf("failed to create file and commit: %v", err)
	}
  fmt.Printf("success: %v ", mr.WebURL)
}

func fileEditCommit(f *Flags, git *gitlab.Client, mr *gitlab.MergeRequest) (newfile *gitlab.FileInfo, err error) {
	// check if file exists, looking for "artifact.txt"
	fileOpts := &gitlab.GetFileOptions{
		Ref: &mr.TargetBranch, // use target branch since it will be default branch and no commits on the new  branch exist yet.
	}
	var env_contents = "CI_APPLICATION_REPOSITORY=" + f.ciRegistryImage + "\nCI_APPLICATION_TAG=" + f.ciCommitTag
	exists := true

	file, _, flowerr := git.RepositoryFiles.GetFile(f.targetProjectId, "handoff.env", fileOpts)
	if flowerr != nil {
		log.Printf("file does not exist: %v", err)
		exists = false
  }
	if exists {
		content_bytes, err := b64.StdEncoding.DecodeString(file.Content)
		if err != nil {
			log.Fatalf("could not decode the file contents: %v", err)
		}
		if string(content_bytes) == env_contents {
			fmt.Println("No need to commit, tag is already in repo")
			return nil, err
		}
		log.Printf("Updating file on branch %v", mr.SourceBranch)
		updatefileOpts := &gitlab.UpdateFileOptions{
			Content:       gitlab.String(env_contents),
			CommitMessage: gitlab.String("update references to " + f.ciCommitTag),
			Branch:        &mr.SourceBranch,
		}
		newfile, _, err := git.RepositoryFiles.UpdateFile(f.targetProjectId, "handoff.env", updatefileOpts)
		if err != nil {
			log.Fatalf("Could not update file: %v", err)
		}
		fmt.Printf("updated file like this: %v", newfile)
	} else {
		log.Printf("Create new file on branch %v", mr.SourceBranch)
		newfileOpts := &gitlab.CreateFileOptions{
			Content:       gitlab.String(env_contents),
			CommitMessage: gitlab.String("update references to " + f.ciCommitTag),
			Branch:        &mr.SourceBranch,
		}
		newfile, _, err := git.RepositoryFiles.CreateFile(f.targetProjectId, "handoff.env", newfileOpts)
		// if it doesn't exist, create the file and make that the contents.
		if err != nil {
			log.Fatalf("Could not create file: %v", err)
			return nil, err
		}
		fmt.Printf("new file created like this: %v", newfile)
  }
	return newfile, err
}

func checkProjects(f *Flags, git *gitlab.Client) (projects []*gitlab.Project, err error) {
	projOpts := &gitlab.ListProjectsOptions{
		Search: gitlab.String(f.ciProjectName),
	}

	projects, _, serr := git.Projects.ListProjects(projOpts)

	if serr != nil {
		err = fmt.Errorf("Failed to retrieve projects: %v", serr)
	}
	return projects, err
}

func makeNewMR(f *Flags, git *gitlab.Client) (mr *gitlab.MergeRequest, err error) {
	branchName := "handoff-" + f.ciCommitTag + "-from-" + f.ciCommitShort
	mrDescription := "Increment dependency on " + f.ciProjectName + " to tag " + f.ciCommitTag
	br := &gitlab.CreateBranchOptions{
		Branch: gitlab.String(branchName),
		Ref:    gitlab.String("master"),
	}
	branch, _, serr := git.Branches.CreateBranch(f.targetProjectId, br)
	if serr != nil {
		err = fmt.Errorf("failed to create branch: %v", serr)
		return nil, err
	}
	fmt.Println("New branch: " + branch.WebURL)
	mr_opts := &gitlab.CreateMergeRequestOptions{
		Title:           gitlab.String("Dependency " + f.ciProjectName + " upgrade to " + f.ciCommitTag),
		Description:     gitlab.String(mrDescription),
		SourceBranch:    gitlab.String(branchName),
		TargetBranch:    gitlab.String("master"),
		AssigneeID:      gitlab.Int(f.targetUserId),
		TargetProjectID: gitlab.Int(f.targetProjectId),
		//    MilestoneID:        gitlab.Int(1),
		RemoveSourceBranch: gitlab.Bool(true),
		Squash:             gitlab.Bool(false),
		AllowCollaboration: gitlab.Bool(true),
	}

	mr, _, serr = git.MergeRequests.CreateMergeRequest(f.targetProjectId, mr_opts)
	if serr != nil {
		err = fmt.Errorf("Failed·to·create·merge request:·%v ", serr)
	}
	fmt.Printf("New Merge Request: %v \r\n\r\n", mr.WebURL)
	return mr, err
}

func SetFlagsFromEnv(fs *flag.FlagSet) (err error) {
	alreadySet := make(map[string]bool)
	fs.Visit(func(f *flag.Flag) {
		alreadySet[f.Name] = true
	})
	fs.VisitAll(func(f *flag.Flag) {
		if !alreadySet[f.Name] {
			key := strings.ToUpper(strings.Replace(f.Name, "-", "_", -1))
			val := os.Getenv(key)
			if val != "" {
				if serr := fs.Set(f.Name, val); serr != nil {
					err = fmt.Errorf("invalid value %q for %s: %v", val, key, serr)
				}
			}
		}
	})
	return err
}
