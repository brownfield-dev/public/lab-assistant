package main

import (
	"crypto/rand"
	"flag"
	"fmt"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/xanzy/go-gitlab"
)

var glToken = flag.String("gl-token", "", "Personal access token for testing")
var commit = strings.ToLower(RandString(21))

func TestCheckProjects(t *testing.T) {
	flags := Flags{
		glAddress:          "https://gitlab.com/api/v4/",
		glToken:            *glToken,
		ciCommitSha:        commit,
		ciCommitShort:      commit[:8],
		ciProjectId:        19762438,
		ciProjectName:      "Unit Test Dependency",
		ciProjectNamespace: "brownfield-dev/intenal",
		gitlabUserId:       6364990,
		targetProjectId:    19762418,
		targetUserId:       6364990,
		ciCommitTag:        "v2.22",
	}

	git, err := gitlab.NewClient(flags.glToken, gitlab.WithBaseURL(flags.glAddress))
	if err != nil {
		t.Errorf("couldn't create gitlab client")
	}
	projects, err := checkProjects(&flags, git)
	assert.NoError(t, err)
	if len(projects) < 1 {
		t.Errorf("no projects found")
	}
}

func TestMakeNewMr(t *testing.T) {
	flags := Flags{
		glAddress:          "https://gitlab.com/api/v4/",
		glToken:            *glToken,
		ciCommitSha:        commit,
		ciCommitShort:      commit[:8],
		ciProjectId:        19762438,
		ciProjectName:      "Test Lab Assistant Dependency",
		ciProjectNamespace: "brownfield-dev/intenal",
		gitlabUserId:       6364990,
		targetProjectId:    19762418,
		targetUserId:       6364990,
		ciCommitTag:        "v2",
	}

	git, err := gitlab.NewClient(flags.glToken, gitlab.WithBaseURL(flags.glAddress))
	if err != nil {
		t.Errorf("couldn't create gitlab client")
	}

	mr, err := makeNewMR(&flags, git)
	assert.NoError(t, err)
	fmt.Printf("merge request: %v", mr)
}

func TestMain(m *testing.M) {
	// flag.Parse()
	os.Exit(m.Run())
}

func RandString(n int) string {
	b := make([]byte, n)
	if _, err := rand.Read(b); err != nil {
		panic(err)
	}
	return fmt.Sprintf("%X", b)
}
