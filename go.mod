module gitlab.com/brownfield-dev/public/lab-assistant

go 1.14

require (
	github.com/coreos/pkg v0.0.0-20180928190104-399ea9e2e55f
	github.com/stretchr/testify v1.6.1
	github.com/xanzy/go-gitlab v0.33.0
)
